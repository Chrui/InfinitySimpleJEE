这是最轻量级的代码生成器，生成物只有大约6.5兆。 
采用Servlet,JSP, JSON, JQuery等简单技术实现，是标准的Model2的MVC设计模式的架构。 程序员的瑞士军刀。是喜欢使用低配服务器的程序员和运维人员的最佳选择。 这是无垠式Java通用代码生成器的最新成员，纤量极速，令人爱不释手。 
1.0版研发代号Ada，纪念世界上第一位程序员。Ada Lovelace。这是无垠式代码生成器系列的第一个正式版。1.0.0版 Ada 已发布。
无垠式代码生成器SimpleJEE版 1.0 Ada 支持一对多，多对多，Excel模板代码生成。其界面是无垠式代码生成器的传统轻量级JSP Json界面的升级版。非常轻，非常快，典型的后端实用主义界面。

Ada Lovelace

![输入图片说明](https://gitee.com/uploads/images/2018/0205/171812_d293d886_1203742.jpeg "ada3_small.jpg")

生成器界面，已支持SGS语法加亮：

![输入图片说明](https://gitee.com/uploads/images/2018/0210/212949_89d02441_1203742.png "ada_beta4.png")

Excel模板代码生成界面：

![输入图片说明](https://gitee.com/uploads/images/2018/0205/172123_432d2571_1203742.png "in2.png")

Excel模板：
![输入图片说明](https://gitee.com/uploads/images/2018/0205/172837_4abacb39_1203742.png "sp1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0205/172849_5a751c6a_1203742.png "sp2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0205/172923_edacc0d1_1203742.png "sp3.png")

代码生成物截图：
![输入图片说明](https://gitee.com/uploads/images/2018/0205/173814_34460546_1203742.png "target.png")

代码生成物，多对多：
![输入图片说明](https://gitee.com/uploads/images/2018/0205/173829_7962d8dc_1203742.png "target2.png")

Ada Lovelace 镇贴专用：

![输入图片说明](http://dl2.iteye.com/upload/attachment/0129/7696/cade4bcf-7175-33df-83aa-dabce16e6034.png "target3.png")

 **软件架构** 
JQuery,JSP,JSON, JavaScript, HTML5, Servlet, JDBC, Mysql/MariaDB
