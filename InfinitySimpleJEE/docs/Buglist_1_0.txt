1.DB名字不对（为项目名，而不是DbName）【已修复】
2.Excel生成没有数据【已修复】
3.SGS生成没有数据
4.还未支持Excel模板自动保存【已修复】
5.尚未迁徙到JQuery Easy UI界面
6.尚未移除传统jsp界面
7.尚未支持一对多
8.尚未支持多对多
9.尚未支持Oracle数据库
10.string已小写开头会导致生成物类型出错
11.类型名String后跟一乱码导致生成物出错
12.log后台中文乱码
13.InfyWater项目中Privilege的url字段缺失【未复现】
14.Dropdown的字段名一定为domainId格式，需加校验【修复】
15.字段名第二个一定要为小写，否则系统会认为第一个字母是大写，需要校验【修复】
16.SQL关键字如Order等要加校验【修复】
17.Excel模板字段名去除空格【修复】
18.首页地址错误【修复】